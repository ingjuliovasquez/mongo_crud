"use client";

import { useState } from "react";
import { useRouter } from "next/navigation";

export default function EditTopicForm({ id, oldTitle, oldAuthor, oldContent }) {
  const [title, setNewTitle] = useState(oldTitle);
  const [author, setNewAuthor] = useState(oldAuthor);
  const [content, setNewContent] = useState(oldContent);

  const router = useRouter();

  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      const res = await fetch(`http://localhost:3000/api/topics/${id}`, {
        method: "PUT",
        headers: {
          "Content-type": "application/json",
        },
        body: JSON.stringify({ title, author, content }),
      });

      if (!res.ok) {
        throw new Error("Error al actualizar la entrada");
      }

      router.refresh();
      router.push("/");
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <form onSubmit={handleSubmit} className="flex flex-col gap-3">
      {/* <span>{id}</span> */}
      <input
        onChange={(e) => setNewTitle(e.target.value)}
        value={title}
        className="border border-slate-500 px-8 py-2"
        type="text"
        placeholder="Título"
      />

      <input
        onChange={(e) => setNewAuthor(e.target.value)}
        value={author}
        className="border border-slate-500 px-8 py-2"
        type="text"
        placeholder="Autor"
      />

      <input
        onChange={(e) => setNewContent(e.target.value)}
        value={content}
        className="border border-slate-500 px-8 py-2"
        type="text"
        placeholder="Contenido"
      />

      <button className="bg-green-600 font-bold text-white py-3 px-6 w-fit">
        Actualizar Entrada
      </button>
    </form>
  );
}

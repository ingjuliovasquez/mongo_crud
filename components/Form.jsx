"use client"
import React from 'react'

import { useEffect, useState } from "react";
import { useParams, useRouter } from "next/navigation";
import db from '@/database/controller'

export default function Form() {
    const [formData, setFormData] = useState({
        titulo: '',
        autor: '',
        contenido: ''
    });

    const params = useParams();
    const router = useRouter();

    useEffect(() => {
        if (params.id) {
            db.getEntrada(params.id)
                .then(entrada => setFormData( entrada ))
                .catch(e => alert(e));
        }
    }, [])

    const handleSubmit = (e) => {
        e.preventDefault();
        if(!formData.id) {
            db.createEntrada(formData)
            .then(() => { router.push('/') })
            .catch(e => alert( e ))
        } else {
            db.updateEntrada(formData.id, formData)
            .then(() => { router.push('/') })
            .catch(e => alert( e ))
        }
    };

    return (
        <form onSubmit={handleSubmit} className="flex flex-col gap-3">
            <input
                onChange={(e) => setFormData({ ...formData, titulo: e.target.value })}
                value={formData.titulo}
                className="border border-slate-500 px-8 py-2"
                type="text"
                placeholder="Título"
                required
            />

            <input
                onChange={(e) => setFormData({ ...formData, autor: e.target.value })}
                value={formData.autor}
                className="border border-slate-500 px-8 py-2"
                type="text"
                placeholder="Autor"
                required
            />

            <textarea
                onChange={(e) => setFormData({ ...formData, contenido: e.target.value })}
                value={formData.contenido}
                className="border border-slate-500 px-8 py-2"
                type="text"
                placeholder="Contenido"
                required
                rows={6}
            />

            <button
                type="submit"
                className="bg-green-600 font-bold text-white py-3 px-6 w-fit"
            >
                Agregar Entrada
            </button>
        </form>
    );
}

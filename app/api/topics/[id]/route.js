import { NextResponse } from "next/server";
import { pool } from '@/configDB/db';

export async function PUT(request, { params }) {
  const { id } = params;
  const { titulo, autor, contenido } = await request.json();
  try {
    await pool.query("UPDATE entrada SET ? WHERE id = ?", [{ titulo, autor, contenido }, id])
    return NextResponse.json({ message: "Entrada actalizada" }, { status: 200 });
  } catch(error) {
    return NextResponse.json({ message: error.message }, { status: 400 });
  }
}

export async function GET(_, { params }) {
  const { id } = params;
  const [topic] = await pool.query("SELECT * FROM entrada WHERE id = ?", [id])
  return NextResponse.json([topic][0][0], { status: 200 });
}

export async function DELETE(_, { params }) {
  const { id } = params;
  try {
    await pool.query("DELETE FROM entrada WHERE id = ?", [id])
    return NextResponse.json({ message: "Entrada eliminada" }, { status: 200 });
  }
  catch (error) {
    return NextResponse.json({ message: error.message }, { status: 400 });
  }


}

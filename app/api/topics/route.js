import { NextResponse } from "next/server";
import { pool } from '@/configDB/db';


export async function POST(request) {
  const { titulo, autor, contenido } = await request.json();
  try {
    await pool.query(`INSERT INTO entrada SET ?`, { titulo, autor, contenido });
    return NextResponse.json({ message: "Entrada guardada" }, { status: 201 });
  }
  catch (error) {
    return NextResponse.json({ message: error.message }, { status: 400 });
  }
}

export async function GET(req) {
  const orderby = req.nextUrl.searchParams.get('orderby');

  let query = "";
  if(orderby) {
    query += `ORDER BY ${orderby} `
  }

  const [rows] = await pool.query(`SELECT * FROM entrada ${query};`)
  return NextResponse.json([rows][0]);
}


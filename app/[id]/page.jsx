"use client"
import { useEffect, useState } from 'react'
import { useParams, useRouter } from 'next/navigation'
import db from '@/database/controller'
import Link from 'next/link'

export default function post() {

    const params = useParams();
    const router = useRouter();

    const [post, setPost] = useState({
        titulo: '',
        autor: '',
        contenido: '',
        publicacion: ''
    })

    useEffect(() => {
        if (params.id) {
            db.getEntrada(params.id)
                .then(_post => setPost(_post))
                .catch(e => {
                    alert(e);
                    router.push('/');
                })
        }
    }, [])

    return (
        <article>
            <Link className="text-blue-600 hover:text-blue-400" href="/" >Ir al inicio</Link>
            <br />
            <br />
            <h1 className='text-4xl'><strong> {post.titulo} </strong></h1>
            <div className="w-full flex justify-between">
                <h2 className='text-xl' >Por {post.autor}</h2>
                <span>{post.publicacion.slice(0, 10)}</span>
            </div>
            <br />
            <br />
            <p>
                {post.contenido}
            </p>
        </article>
    )
}

import axios from "axios"

const endpoint = "/api/topics/"

function getEntradas(orderby = null) {
    return new Promise( async ( resolve, reject ) => {
        try {
            const { data } = await axios.get( endpoint, {params: { orderby }} );
            resolve( data )
        } catch( error ) {
            reject(new Error( error ))
        }
    })
}

function getEntrada(id) {
    return new Promise( async ( resolve, reject ) => {
        try {
            const { data } = await axios.get(endpoint + id);
            resolve( data );
        } catch( error ) {
            reject(new Error( error ));
        }
    })
}

function createEntrada(payload) {
    return new Promise( async (resolve, reject) => {
        try {
            const { data } = await axios.post( endpoint, payload )
            resolve( data )
        } catch( error ) {
            reject(new Error(error))
        }
    })
}

function updateEntrada(id, payload) {
    return new Promise( async ( resolve, reject ) => {
        try {
            const { data } = await axios.put(endpoint + id, payload);
            resolve(data);
        } catch( error ) {
            reject( new Error( error ));
        }
    })
}

function deleteEntrada(id) {
    return new Promise( async (resolve, reject) => {
        try {
            const { data } = await axios.delete(endpoint + id);
            resolve( data );
        } catch(error) {
            reject(new Error( error ));
        }
    })
}

export default {
    getEntrada, getEntradas, createEntrada, updateEntrada, deleteEntrada
}